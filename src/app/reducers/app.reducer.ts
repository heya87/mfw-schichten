import {ActionReducerMap} from '@ngrx/store';
import {ressortReducer} from '../ressort/state/ressort.reducer';
import {shiftReducer} from '../shift/state/shift.reducer';
import {calendarReducer} from '../calendar/state/calendar.reducer';
import {coreReducer} from '../core/state/core.reducer';
import {usersReducer} from '../users/state/users.reducer';

export const reducers: ActionReducerMap<any> = {
  ressort: ressortReducer,
  shift: shiftReducer,
  calendar: calendarReducer,
  core: coreReducer,
  users: usersReducer
};
