import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {shiftRoutesNames} from './shift/shift.route.names';
import {userProfileRoutesNames} from './user-profile/user-profile.route.names';
import {authRoutesNames} from './authentication/auth.route.names';


const routes: Routes = [
  {
    path: shiftRoutesNames.DETAIL, loadChildren: './shift/shift.module#ShiftModule', pathMatch: 'full',
  },
  {
    path: userProfileRoutesNames.BASE_FULL, loadChildren: './user-profile/user-profile.module#UserProfileModule'
  },
  {
    path: authRoutesNames.BASE, loadChildren: './authentication/authentication.module#AuthenticationModule'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
