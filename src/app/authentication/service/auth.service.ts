import {Injectable} from '@angular/core';
import {User} from '../../models/user';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import {authRoutesNames} from '../auth.route.names';
import {defer, from, Observable, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromCore from '../../core/state/core.state';
import * as CoreActions from '../../core/state/core.action';
import {map} from 'rxjs/operators';
import {User as fireBaseUser} from 'firebase/app';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private subscription: Subscription[] = [];
  private collectionName = 'users';

  constructor(
    protected afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    private store: Store<fromCore.State>,
  ) {

    afAuth.auth.onAuthStateChanged((user?: fireBaseUser) => {
      if (user !== null) {
        this.store.dispatch(CoreActions.UserChanged({user}));
        this.subscribeForUser(user.uid);
      } else {
        this.clearSubscription();
      }
    });
  }

  subscribeForUser(id) {
    this.addSubscription(this.afs.collection(this.collectionName).doc<User>(id)
      .valueChanges()
      .pipe(map(user => {
        if (user) {
          return {id: user.uid, ...user} as User;
        } else {
          return {};
        }
      }))
      .subscribe((user: User) => {
        this.metadataChanged(user);
      }));
  }

  private metadataChanged(user: User) {
    this.store.dispatch(CoreActions.MetadataChanged({user}));
  }

  addSubscription(subscription: Subscription) {
    this.subscription.push(subscription);
  }

  clearSubscription() {
    this.subscription.forEach(x => x.unsubscribe());
    this.subscription = [];
  }


  login(email, password): Observable<fireBaseUser> {
    return defer(async () => {
      await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      return this.afAuth.auth.currentUser;
    });
  }

  signUp(user: User): Observable<User> {
    return defer(async () => {
      const userCredential = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      await this.afAuth.auth.currentUser.sendEmailVerification();
      user.uid = userCredential.user.uid;
      return user;
    });
  }

  signOut(): Observable<void> {
    return defer(async () => {
      this.clearSubscription();
      await this.afAuth.auth.signOut();
    });
  }

  // Send email verfificaiton when new user sign up
  sendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.router.navigate([authRoutesNames.VERIFY_EMAIL]);
      });
  }

  // Reset Forggot password
  resetPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
      .catch(error => error);
  }

  createMetadataForUser(user: User): Observable<void> {
    return defer(async () => {
      from(this.afs.collection(this.collectionName).doc(user.uid).set(user));
    });
  }

  updateMetadataForUser(user: User): Observable<void> {
    return defer(async () => {
      from(this.getDoc(user.uid).update(user));
    });
  }

  getDoc(id: string) {
    return this.afs.doc(`${this.collectionName}/${id}`);
  }
}
