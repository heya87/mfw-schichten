import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../service/auth.service';
import {select, Store} from '@ngrx/store';
import * as fromCore from '../../core/state/core.state';
import {map, skipWhile} from 'rxjs/operators';
import {ressortRoutesNames} from '../../ressort/ressorts.route.names';


@Injectable({
  providedIn: 'root'
})

export class SecureInnerPagesGuard implements CanActivate {

  constructor(
    public authService: AuthService,
    public router: Router,
    public store: Store<fromCore.State>,
  ) {
  }

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromCore.selectCore),
      map(user => {
        if (user.user && user.user.emailVerified) {
          this.router.navigate([ressortRoutesNames.BASE]);
        } else {
          return true;
        }
      })
    );
  }
}
