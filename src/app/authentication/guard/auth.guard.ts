import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../service/auth.service';
import {map, take} from 'rxjs/operators';
import {authRoutesNames} from '../auth.route.names';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(
    public afAuth: AngularFireAuth,
    public authService: AuthService,
    public router: Router,
  ) {
  }

  canActivate(): Observable<boolean> {
    return this.afAuth.authState.pipe(
      take(1),
      map((authState) => {
        const isLoggedIn = !!authState;

        if (!isLoggedIn) {
          this.router.navigate([authRoutesNames.ACCESS_DENIED]);
        } else if (!authState.emailVerified) {
          this.router.navigate([authRoutesNames.VERIFY_EMAIL]);
        }
        return isLoggedIn;
      })
    );
  }
}
