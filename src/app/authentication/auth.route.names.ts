export const authRoutesNames = {
  BASE: 'auth',

  SUB_SIGN_IN: 'sign-in',
  SIGN_IN: 'auth/sign-in',

  SUB_SIGN_UP: 'sign-up',
  SIGN_UP: 'auth/sign-up',

  SUB_VERIFY_EMAIL: 'verify-email',
  VERIFY_EMAIL: 'auth/verify-email',

  SUB_RESET_PASSWORD: 'reset-password',
  RESET_PASSWORD: 'auth/reset-password',

  ACCESS_DENIED: 'auth/access-denied',
  SUB_ACCESS_DENIED: 'access-denied',
};

