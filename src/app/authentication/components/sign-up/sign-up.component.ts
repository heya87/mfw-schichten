import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../service/auth.service';
import {User} from 'src/app/models/user';
import * as CoreActions from '../../../core/state/core.action';
import {Store} from '@ngrx/store';
import * as fromCore from '../../../core/state/core.state';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html'
})
export class SignUpComponent implements OnInit {

  public hide = true;
  private maxChars = 200;

  public form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    firstName: new FormControl('', [Validators.required, Validators.maxLength(this.maxChars)]),
    lastName: new FormControl('', [Validators.required, Validators.maxLength(this.maxChars)]),
    address: new FormControl('', Validators.maxLength(this.maxChars)),
    place: new FormControl('', Validators.maxLength(this.maxChars)),
    birthDate: new FormControl('', [Validators.required,
      Validators.pattern('^\\s*(3[01]|[12][0-9]|0?[1-9])\\.(1[012]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$')]),
    phone: new FormControl('', [Validators.required, Validators.pattern('\\b\\d(?: ?\\d){9,12}\\b')]),
    policy: new FormControl('', Validators.required),
  }, {updateOn: 'change'});

  constructor(
    public authService: AuthService,
    private store: Store<fromCore.State>,
  ) {
  }

  ngOnInit() {
  }

  public get emailControl(): AbstractControl {
    return this.form.get('email');
  }

  public get passwordControl(): AbstractControl {
    return this.form.get('password');
  }

  public get firstNameControl(): AbstractControl {
    return this.form.get('firstName');
  }

  public get lastNameControl(): AbstractControl {
    return this.form.get('lastName');
  }

  public get birthDateControl(): AbstractControl {
    return this.form.get('birthDate');
  }

  public get phoneControl(): AbstractControl {
    return this.form.get('phone');
  }

  public get addressControl(): AbstractControl {
    return this.form.get('address');
  }

  public get placeControl(): AbstractControl {
    return this.form.get('place');
  }

  public get policyControl(): AbstractControl {
    return this.form.get('policy');
  }

  public get errorMessageEmail() {

    let errorMessage: string;

    if (this.emailControl.hasError('required')) {
      errorMessage = 'Bitte eine Email angeben.';
    } else if (this.emailControl.hasError('email')) {
      errorMessage = 'Bitte eine gültige Email angeben.';
    } else if (this.emailControl.hasError('minLength')) {
      errorMessage = 'Das Passwort muss minimal sechs Zeichen beinhalten.';
    }
    return errorMessage;
  }

  public get errorMessageFirstName() {
    let errorMessage: string;
    if (this.lastNameControl.hasError('required')) {
      errorMessage = 'Bitte gib deinen  Vornamen ein.';
    } else if (this.lastNameControl.hasError('maxlength')) {
      errorMessage = this.getErrorMessageTooManyChars();
    }
    return errorMessage;
  }

  public get errorMessageLastName() {
    let errorMessage: string;
    if (this.lastNameControl.hasError('required')) {
      errorMessage = 'Bitte gib deinen  Nachnamen ein.';
    } else if (this.lastNameControl.hasError('maxlength')) {
      errorMessage = this.getErrorMessageTooManyChars();
    }
    return errorMessage;
  }

  public get errorMessageBirthDate() {
    let errorMessage: string;
    if (this.birthDateControl.hasError('required')) {
      errorMessage = 'Bitte gib dein Geburtsdatum ein.';
    } else if (this.birthDateControl.hasError('pattern')) {
      errorMessage = 'Bitte gib ein gültiges Geburtsdatum ein.';
    }
    return errorMessage;
  }

  public get errorMessagePassword() {
    let errorMessage: string;
    if (this.passwordControl.hasError('required')) {
      errorMessage = 'Bitte ein Passwort angeben.';
    } else if (this.passwordControl.hasError('minlength')) {
      errorMessage = 'Das Passwort muss minimal sechs Zeichen beinhalten.';
    }
    return errorMessage;
  }

  public get errorMessagePhone() {
    let errorMessage: string;
    if (this.phoneControl.hasError('required')) {
      errorMessage = 'Bitte eine Telefonnummer angeben.';
    } else if (this.phoneControl.hasError('pattern')) {
      errorMessage = 'Bitte gib eine gültige Nummer ein.';
    }
    return errorMessage;
  }

  public get errorMessageAddress() {
    return this.addressControl.hasError('maxlength') ? this.getErrorMessageTooManyChars() : '';
  }

  public get errorMessagePlace() {
    return this.placeControl.hasError('maxlength') ? this.getErrorMessageTooManyChars() : '';
  }

  private getErrorMessageTooManyChars(): string {
    return `Zu viele Zeichen (Maximal  ${this.maxChars} Zeichen).`;
  }

  async submit() {
    const userData: User = {
      uid: null,
      email: this.form.value.email,
      displayName: null,
      photoURL: null,
      emailVerified: null,
      isAdmin: false,
      password: this.form.value.password,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      address: this.form.value.address,
      place: this.form.value.place,
      birthDate: this.form.value.birthDate,
      phone: this.form.value.phone,
      publicTransport: null,
      materialTransport: null,
      peopleTransport: null,
      flyerArea: null,
      hasCar: null,
      lastRessort: null
    };

    this.form.disable();
    await this.store.dispatch(CoreActions.SignUpUser({userData}));
    this.form.enable();
  }

  isInvalid(value) {
    return this.form.controls[value].invalid
      && (this.form.controls[value].dirty || this.form.controls[value].touched);
  }
}
