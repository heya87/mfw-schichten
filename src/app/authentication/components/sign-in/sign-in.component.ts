import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromCore from '../../../core/state/core.state';
import * as CoreActions from '../../../core/state/core.action';
import {authRoutesNames} from '../../auth.route.names';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html'
})
export class SignInComponent implements OnInit {

  constructor(
    public router: Router,
    public authService: AuthService,
    public store: Store<fromCore.State>,
    ) { }

  public form: FormGroup;
  public currentUser;
  public hide = true;
  public authRoutesNames = authRoutesNames;
  public loginErrorMessage: string;


  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    }, { updateOn: 'change' });
  }

  public get emailControl(): AbstractControl {
    return this.form.get('email');
  }

  public get passwordControl(): AbstractControl {
    return this.form.get('password');
  }

  public get errorMessageEmail() {

    let errorMessage: string;

    if (this.emailControl.hasError('required')) {
      errorMessage = 'Bitte eine Email angeben';
    } else if (this.emailControl.hasError('email')) {
      errorMessage = 'Bitte eine gültige Email angeben';
    }

    return errorMessage;

  }

  public get errorMessagePassword() {
    return this.passwordControl.hasError('required') ? 'Bitte gib ein Passwort ein' : '';
  }

  async loginUser(email, password) {
    return this.store.dispatch(CoreActions.LoginUser({email, password}));
  }

  async submit() {
    this.form.disable();
    this.currentUser = await this.loginUser(this.form.value.email, this.form.value.password);

    this.loginErrorMessage = 'Ungültiger Loginversuch. Überprüfe deine Angaben.';

    this.form.enable();
  }

  isInvalid(value) {
    return this.form.controls[value].invalid
      && (this.form.controls[value].dirty || this.form.controls[value].touched);
  }
}
