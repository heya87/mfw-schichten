import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserProfileComponent} from './components/display-user-profile/user-profile.component';
import {EditUserProfileComponent} from './components/edit-user-profile/edit-user-profile.component';
import {userProfileRoutesNames} from './user-profile.route.names';
import {AuthGuard} from '../authentication/guard/auth.guard';
import {MyShiftsComponent} from './components/my-shifts/my-shifts.component';
import {EditAdditionalFieldsWrapperComponent} from './components/edit-additional-fields/edit-additional-fields.component';

const routes: Routes = [

  { path: '', component: UserProfileComponent, canActivate: [AuthGuard]},
  { path: userProfileRoutesNames.EDIT_SUB, component: EditUserProfileComponent, canActivate: [AuthGuard]},
  { path: userProfileRoutesNames.MY_SHIFTS_SUB, component: MyShiftsComponent, canActivate: [AuthGuard]},
  { path: userProfileRoutesNames.EDIT_ADDITIONAL_SUB, component: EditAdditionalFieldsWrapperComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule { }
