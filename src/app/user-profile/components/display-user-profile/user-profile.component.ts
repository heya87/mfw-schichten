import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCore from '../../../core/state/core.state';
import {Store} from '@ngrx/store';
import {User} from '../../../models/user';
import {userProfileRoutesNames} from '../../user-profile.route.names';
import {AuthService} from '../../../authentication/service/auth.service';
import {Observable} from 'rxjs';
import * as CoreActions from '../../../core/state/core.action';
import {additionalFieldInitData} from '../../../shared/additionalFields';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  public user$: Observable<User>;
  public passwordReset = false;

  public additionalFieldsInitData = additionalFieldInitData;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromCore.State>,
    public authService: AuthService,
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('uid');
    if (id) {
      this.user$ = this.store.select(fromCore.selectMetadata);
    }
  }

  public navigateToDetailView(id: string) {
    const path = userProfileRoutesNames.EDIT_FULL.replace(':uid', id);
    this.router.navigate([path]);
  }

  public resetPassword(email: string, e) {

    e.preventDefault();
    this.passwordReset = true;
    return this.store.dispatch(CoreActions.ResetPassword({email}));

  }
}
