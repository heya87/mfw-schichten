import {Component, Input, OnInit} from '@angular/core';
import {additionalFieldInitData} from '../../../shared/additionalFields';
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-additional-fields',
  templateUrl: './additional-fields.component.html'
})
export class AdditionalFieldsComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() requiredAdditionalFields: any;

  public additionalFields: object[];


  constructor(

  ) {

  this.additionalFields = additionalFieldInitData;

  }

  ngOnInit() {


  }

  public isRequired(additionalField) {

    let required = false;

    if (this.requiredAdditionalFields) {

      this.requiredAdditionalFields.filter(requiredAdditionalField => requiredAdditionalField.id === additionalField.id).length ? required = true : required = false;

    }

    return required;

  }




}


