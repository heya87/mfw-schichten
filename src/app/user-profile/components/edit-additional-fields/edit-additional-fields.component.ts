import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromCore from '../../../core/state/core.state';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {User} from '../../../models/user';
import * as CoreActions from '../../../core/state/core.action';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as fromRessort from '../../../ressort/state/ressort.state';
import {Ressort} from '../../../models/ressorts';
import {additionalFieldInitData} from '../../../shared/additionalFields';
import {ressortRoutesNames} from '../../../ressort/ressorts.route.names';

@Component({
  selector: 'app-edit-additional-fields',
  templateUrl: './edit-additional-fields.component.html'
})
export class EditAdditionalFieldsComponent implements OnInit {

  public _user;
  public form: FormGroup;
  public ressort$: Observable<Ressort>;
  private maxChars =  200;

  @Input()
  public set user(value) {
    this._user = value;
    this.ngOnInit();
  }

  public get user() {
    return this._user;
  }

  constructor(
    private router: Router,
    private store: Store<fromCore.State>,
    private fb: FormBuilder,
    private ressortStore: Store<fromRessort.RessortState>,

  ) {
  }

  ngOnInit() {

    this.ressort$ = this.ressortStore.select(fromRessort.selectRessort(this.user.lastRessort));


    this.buildAdditionalFields();

  }

  public buildAdditionalFields() {

    const group = {};

    additionalFieldInitData.forEach( field => {
      group[field.id] = new FormControl(this.user[field.id] ? this.user[field.id] : '', [Validators.required]);
    });

    this.form = new FormGroup(group);
  }

  public isRequired(ressort) {

    ressort.additionalFields.map( (additionalFieldSelected, index) => additionalFieldInitData[index].selected = additionalFieldSelected );
    const requiredAdditionalFields = additionalFieldInitData.filter( additionalFieldsSelected => additionalFieldsSelected.selected );

    return requiredAdditionalFields;
  }


  public getErrorMessageTooManyChars(): string {
    return `Zu viele Zeichen (Maximal  ${this.maxChars} Zeichen).`;
  }


  isInvalid(value) {
    return this.form.controls[value].invalid
      && (this.form.controls[value].dirty || this.form.controls[value].touched);
  }

  async updateUser(ressort) {

    const ressortAdditionalFields = additionalFieldInitData;
    ressort.additionalFields.map( (additionalFieldSelected, index) => ressortAdditionalFields[index].selected = additionalFieldSelected );
    const requiredAdditionalFields = ressortAdditionalFields.filter( additionalFieldsSelected => additionalFieldsSelected.selected );

    const updatedFields = {};

    requiredAdditionalFields.map(field => {
      updatedFields[field.id] = this.form.value[field.id];
    });


    const user: User = {
      ...this.user,
      ...updatedFields,
      lastRessort: null
    };
    return this.store.dispatch(CoreActions.UpdateMetadataForUser({user}));
  }


  async onSubmit(ressort) {
    this.form.disable();
    this.updateUser(ressort);
    this.form.enable();
    this.navigateToRessort(ressort.id);
  }


  private navigateToRessort(id: string) {
    const path = ressortRoutesNames.DETAIL.replace(':id', id);
    this.router.navigate([path]);
  }

}

@Component({
  selector: 'app-edit-additional-fields-wrapper',
  template: `
      <app-edit-additional-fields *ngIf="user$ | async" [user]="user$ | async"></app-edit-additional-fields>
  `,
})

export class EditAdditionalFieldsWrapperComponent implements OnInit {

  public user$: Observable<User>;

  constructor(
    private store: Store<fromCore.State>,
    private route: ActivatedRoute,
  ) {
    const id = this.route.snapshot.paramMap.get('uid');
    if (id) {
      this.user$ = this.store.select(
        fromCore.selectMetadata);
    }
  }

  ngOnInit() {
  }
}

