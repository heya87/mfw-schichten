export const userProfileRoutesNames = {
  BASE: 'profile/',
  BASE_FULL: 'profile/:uid',
  EDIT_FULL: 'profile/:uid/edit',
  EDIT_SUB: 'edit',
  MY_SHIFTS_FULL: 'profile/:uid/my-shifts',
  MY_SHIFTS_SUB: 'my-shifts',
  EDIT_ADDITIONAL_FULL: 'profile/:uid/additional-info',
  EDIT_ADDITIONAL_SUB: 'additional-info'
};
