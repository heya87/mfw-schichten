import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../authentication/service/auth.service';
import {authRoutesNames} from '../../../authentication/auth.route.names';
import {userProfileRoutesNames} from '../../../user-profile/user-profile.route.names';
import {Store} from '@ngrx/store';
import * as fromCore from '../../../core/state/core.state';
import * as CoreActions from '../../../core/state/core.action';
import {User as fireBaseUser} from 'firebase/app';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.scss']
})

export class AppNavComponent implements OnInit {

  public authRoutesNames = authRoutesNames;
  public userProfileRoutesNames = userProfileRoutesNames;

  public user$: Observable<fireBaseUser>;
  public isLoggedIn$: Observable<boolean>;
  public isAdmin$: Observable<boolean>;

  constructor(
    public authService: AuthService,
    private store: Store<fromCore.State>
  ) {
  }

  ngOnInit() {
    this.user$ = this.store.select(fromCore.selectUser);
    this.isLoggedIn$ = this.store.select(fromCore.isLoggedIn);
    this.isAdmin$ = this.store.select(fromCore.isAdmin);
  }

  signOut() {
    this.store.dispatch(CoreActions.SignOutUser({}));
  }
}
