import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';
import {MediaMatcher} from '@angular/cdk/layout';
import {NavigationEnd, Router} from '@angular/router';
import {ressortRoutesNames} from '../../../ressort/ressorts.route.names';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})


export class HeaderComponent implements OnInit {

  navActive = false;
  ressortRoutesNames = ressortRoutesNames;

  constructor(

    private changeDetectionRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private router: Router

  ) {

    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.navActive) {
          this.toggleNav();
        }
      }

    });

  }

  ngOnInit() {
  }

  toggleNav() {

    this.navActive = !this.navActive;

    if (this.navActive) {
      disableBodyScroll();
    } else {
      enableBodyScroll();
    }

  }

}
