import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {ressortRoutesNames} from '../../../ressort/ressorts.route.names';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.scss']
})
export class BackButtonComponent implements OnInit {

  constructor(
    private location: Location,
    private router: Router,
  ) {  }

  ngOnInit() {
  }

  public navigateBack() {
    if (this.router.navigated) {
      this.location.back();
    } else {
      this.router.navigate([ressortRoutesNames.BASE]);
    }
  }

}
