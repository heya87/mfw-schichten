import {AdditionalFields} from '../models/additionalFields';

export const additionalFieldInitData: AdditionalFields[] = [
  {
    name: 'ÖV-Billet',
    selected: false,
    id: 'publicTransport',
    icon: 'train',
    options: {
      ga: 'GA',
      halbtax: '1/2-Tax',
      alleZonen: 'ZVV alle Zonen',
      andere: 'andere',
      keins: 'keins'
    }
  },
  {
    name: 'Materialtransport',
    selected: false,
    id: 'materialTransport',
    icon: 'local_shipping',
    options: {
      b: 'B',
      be: 'BE',
      C1: 'C1',
      C1E: 'C1E',
      C: 'C',
      CE: 'CE',
      none: 'vo hand'
    }
  },
  {
    name: 'Personentransport',
    selected: false,
    id: 'peopleTransport',
    icon: 'airport_shuttle',
    options: {
      b: 'B',
      be: 'BE',
      d1: 'D1',
      d1e: 'D1E',
      d: 'D',
      de: 'DE',
      none: 'huckepack'
    }
  },
  {
    name: 'Verfügbarkeit Auto',
    selected: false,
    id: 'hasCar',
    icon: 'directions_car',
    options: {
      owns: 'Besitze ein Auto',
      canGet: 'Kann jederzeit ein Auto beschaffen',
      none: 'Nix da Auto!'
    }
  },
  {
    name: 'Gabelstapler',
    selected: false,
    id: 'canForkLift',
    icon: 'child_friendly',
    options: {
      yes: 'Kann gabelstapeln',
      no: 'Kann ich nicht'
    }
  },
  {
    name: 'Flyern Gebiet',
    selected: false,
    id: 'flyerArea',
    icon: 'zoom_out_map'
  }
];
