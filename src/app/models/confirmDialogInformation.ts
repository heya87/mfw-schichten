export interface ConfirmDialogInformation {
  title: string;
  message: string;
}
