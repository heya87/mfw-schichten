export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  isAdmin: boolean;
  password?: string;
  lastName?: string;
  firstName?: string;
  address?: string;
  place?: string;
  birthDate?: string;
  phone?: number;
  publicTransport?: string;
  materialTransport?: string;
  peopleTransport?: string;
  flyerArea?: string;
  hasCar?: boolean;
  lastRessort?: string;
}
