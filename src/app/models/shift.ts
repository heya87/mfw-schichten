import * as firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;


export interface Shift {
  id: string;
  start: Timestamp;
  end: Timestamp;
  noOfWorker: number;
  ressortId: string;
  workers: {id: string, confirmed: boolean}[];
  confirmed: boolean;
  title?: string;
}
