export interface AdditionalFields {
  name: string;
  selected: boolean;
  id: string;
  icon: string;
  options?: { [key: string]: string};
}
