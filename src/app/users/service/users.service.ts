import {Injectable} from '@angular/core';
import {User} from '../../models/user';
import {AngularFirestore} from '@angular/fire/firestore';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {map} from 'rxjs/operators';
import * as UsersActions from '../state/users.action';
import * as fromUsers from '../state/users.state';

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  private subscription: Subscription[] = [];
  private collectionName = 'users';


  constructor(
    protected afs: AngularFirestore,
    public router: Router,
    private store: Store<fromUsers.UsersState>,
  ) {
    this.addSubscription(afs.collection<User>(this.collectionName)
      .snapshotChanges()
      .pipe(map(actions => {
        return actions.map(action => {
          return {id: action.payload.doc.id, ...action.payload.doc.data()} as User;
        });
      })).subscribe((items) => this.usersChanged(items)));

  }


  addSubscription(subscription: Subscription) {
    this.subscription.push(subscription);
  }

  clearSubscription() {
    this.subscription.forEach(x => x.unsubscribe());
    this.subscription = [];
  }

  async usersChanged(users) {
    await this.store.dispatch(UsersActions.UsersChanged({users}));
  }


  getDoc(id: string) {
    return this.afs.doc(`${this.collectionName}/${id}`);
  }

  async getCollection() {
    return this.afs.collection(this.collectionName);
  }

  getUser(id: string) {
    const docRef = this.afs.collection('users').doc(id);
    return docRef.ref.get().then((doc) => {
      const currentUser = {id: doc.data().uid, ...doc.data()};
      return currentUser;
    });
  }


}
