import {createAction, props} from "@ngrx/store";
import {User} from '../../models/user';


const GET_USERS = '[Users] Get'
const USERS_CHANGED = '[Users] changed'
const GET_USERS_BY_ID = '[Users] Get by ids'

export const UsersChanged = createAction(USERS_CHANGED, props<{ users: User[] }>());

export const GetUser = createAction(GET_USERS, props<{user: User}>());

export const GetUsersById = createAction(GET_USERS_BY_ID, props<{user: User[]}>());
