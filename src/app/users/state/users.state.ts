import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {User} from '../../models/user';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export const usersAdapter = createEntityAdapter<User>();

export interface UsersState extends EntityState<User> {
}

export const initialState: UsersState = usersAdapter.getInitialState();

export const getUserState = createFeatureSelector<UsersState>('users');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = usersAdapter.getSelectors(getUserState);


export const selectUserById = userId => {
  return createSelector(selectAll, x => x.find(user => user.uid === userId));
}

export const selectUsersById = userIds => {
  return createSelector(selectAll, x => {
    const users = [];
    userIds.map(userId => {
      users.push(x.find(user => user.uid === userId));
    });
    return users;
  });
};


export const selectUsersByShift = shifts => {
  return createSelector(selectAll, x => {

    const users = [];

    shifts.map(shift => {
      const shiftExport = {
        shift,
        workers: []
      };

      shift.workers.map(worker => {
        shiftExport.workers.push(x.find(user => user.uid === worker.id));
      });
      users.push(shiftExport);
    });

    return users;
  });
};
