import {Component, Input, OnInit} from '@angular/core';
import * as ShiftActions from '../../../shift/state/shift.action';
import * as fromShift from '../../../shift/state/shift.state';
import {Store} from '@ngrx/store';
import {Shift} from '../../../models/shift';
import {Observable} from 'rxjs';
import * as fromUsers from '../../state/users.state';
import {User} from '../../../models/user';
import {ConfirmDialogInformation} from '../../../models/confirmDialogInformation';
import {ConfirmationDialogComponent} from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';


@Component({
  selector: 'app-users-worker-list',
  templateUrl: './users-worker-list.component.html',
  styleUrls: ['./users-worker-list.component.scss']
})
export class UsersWorkerListComponent implements OnInit {


  @Input('worker')
  public worker;

  @Input('shift')
  public shift: Shift;

  public user$: Observable<User>;

  constructor(
    private store: Store<fromShift.ShiftState>,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.user$ = this.store.select(fromUsers.selectUserById(this.worker.id));
  }

  public toggleConfirmation(shift, worker) {

    const toggledWorker = shift.workers.findIndex(x => x.id === worker.id);
    shift.workers[toggledWorker].confirmed = !shift.workers[toggledWorker].confirmed;

    this.store.dispatch(ShiftActions.UpdateShift({shift}));

  }


  private unRegisterFromShift(shift, userId) {
    const workers = shift.workers.filter( worker => worker.id !== userId);
    this.updateShiftWorkers(shift, workers);
  }

  private updateShiftWorkers(shift, workers) {
    let updatedShift: Shift;
    updatedShift = {
      ...shift,
      workers
    };
    this.store.dispatch(ShiftActions.UpdateShift({shift: updatedShift}));
  }

  openDialog(shift: Shift, user: User): void {
    const popupTitle = `Helfer abmelden`;
    const popupMessage = `Willst du den Helfer ${user.firstName} ${user.lastName} wirklich von dieser Schicht abmelden?`;
    const dialogInformation: ConfirmDialogInformation = {
      title: popupTitle,
      message: popupMessage
    };
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: dialogInformation,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
          this.unRegisterFromShift(shift, user.uid);
      }
    });
  }

}
