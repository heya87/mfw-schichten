import {Action, createReducer, on} from '@ngrx/store';
import * as coreActions from './core.action';
import {initialState, State} from './core.state';


export function coreReducer(state: State| undefined, action: Action) {
  return reducer(state, action);
}

const reducer = createReducer(
  initialState,
  on(coreActions.UserChanged, (state, action) => ({...state, user: action.user})),
  on(coreActions.AddMessage, (state, action) => (
    {...state, messages: [...state.messages, {id : new Date().toISOString(), message: action.message, type: action.type}]})),
  on(coreActions.RemoveMessage, (state, action) => (
    {...state, messages: state.messages.filter(x => x.id !== action.item.id)})),
  on(coreActions.MetadataChanged, (state, action) => ({...state, metadata: action.user})),
);
