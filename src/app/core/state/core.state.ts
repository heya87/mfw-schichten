import {createFeatureSelector, createSelector} from '@ngrx/store';
import {User as fireBaseUser} from 'firebase/app';
import {User} from '../../models/user';

export interface State {
  user: fireBaseUser;
  metadata: User;
  messages: any[];
}

export const initialState: State = {
  user: null,
  metadata: null,
  messages: [],
};

export const selectCore = createFeatureSelector<State>('core');
export const selectUser = createSelector(selectCore, x => x.user);
export const selectMetadata = createSelector(selectCore, x => x.metadata);
export const selectMessages = createSelector(selectCore, x => x.messages);
export const isLoggedIn = createSelector(selectCore, x => x.user && x.user.emailVerified);
export const isAdmin = createSelector(selectCore, x => x.metadata && x.metadata.isAdmin);

