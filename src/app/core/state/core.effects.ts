import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, flatMap, map, switchMap} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import * as fromCore from '../state/core.state';
import * as CoreActions from '../state/core.action';
import {AuthService} from 'src/app/authentication/service/auth.service';
import {Router} from '@angular/router';
import {authRoutesNames} from '../../authentication/auth.route.names';
import {of} from 'rxjs';

@Injectable()
export class CoreEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    public store: Store<fromCore.State>,
    public  router: Router,
  ) {
  }

  @Effect({dispatch: false})
  signUp$ = this.actions$.pipe(
    ofType(CoreActions.SignUpUser),
    map(action => action),
    switchMap(action => this.authService.signUp(action.userData)),
    switchMap(user => [
      this.store.dispatch(CoreActions.CreateMetadataForUser({user})),
      this.router.navigate([authRoutesNames.VERIFY_EMAIL])
    ]),
    catchError((error) =>
      of(this.handleError(error)
      )
    )
  );

  @Effect({dispatch: false})
  login$ = this.actions$.pipe(
    ofType(CoreActions.LoginUser),
    switchMap((data) => {
      const messageIllegalLoginData = 'Ungültige Login Daten. Bitte überprüfen Sie ihre Angaben oder erstellen Sie einen Benutzer.';
      const messageMailNotVerified = 'Sie haben Ihre Mail Adresse noch nicht verifiziert.';

      return this.authService.login(data.email, data.password).pipe(
        map(user => {
          if (user.emailVerified) {
            // UserChanged needs to be called here additionally to the onAuthChanged call because onAuthChanged is not
            // called when the email is verified by the user. So if the user tries to login after he created an
            // account and verified his mail, the user is not updated in the store and the state is not in sync
            // with firestore -> because of this UserChanged is called twice on further log-ins.
            this.store.dispatch(CoreActions.UserChanged({user}));
            this.router.navigate(['/']);
          } else if (!user.emailVerified) {
            this.store.dispatch(CoreActions.AddMessage({message: messageMailNotVerified}));
            this.router.navigate([authRoutesNames.VERIFY_EMAIL]);
          } else {
            this.store.dispatch(CoreActions.AddMessage({message: messageIllegalLoginData}));
            this.router.navigate([authRoutesNames.SIGN_UP]);
          }
        }),
        catchError(() =>
          of(this.store.dispatch(
            CoreActions.AddMessage({message: messageIllegalLoginData})
          ))));
    }),
  );

  @Effect({dispatch: false})
  signOut$ = this.actions$.pipe(
    ofType(CoreActions.SignOutUser),
    map(action => action),
    switchMap(() => this.authService.signOut()),
    switchMap(() => [
      this.store.dispatch(CoreActions.UserChanged({user: null})),
      this.store.dispatch(CoreActions.MetadataChanged({user: null})),
      this.router.navigate(['/'])
    ])
  );

  @Effect({dispatch: false})
  createMetadataForUser = this.actions$.pipe(
    ofType(CoreActions.CreateMetadataForUser),
    flatMap(data => {
      return this.authService.createMetadataForUser(data.user);
    })
  );

  @Effect({dispatch: false})
  updateMetadataForUser = this.actions$.pipe(
    ofType(CoreActions.UpdateMetadataForUser),
    flatMap(data => {
      return this.authService.updateMetadataForUser(data.user);
    })
  );

  @Effect({dispatch: false})
  resetPassword$ = this.actions$.pipe(
    ofType(CoreActions.ResetPassword),
    flatMap(action => {
      return this.authService.resetPassword(action.email);
    })
  );

  private handleError(error: any) {
    if (error.code === 'auth/email-already-in-use') {
      this.router.navigate([authRoutesNames.VERIFY_EMAIL]);
    } else {
      this.store.dispatch(
        CoreActions.AddMessage({message: error.toString()})
      );
    }
  }
}
