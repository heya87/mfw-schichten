import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {Ressort} from '../../models/ressorts';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export const ressortAdapter = createEntityAdapter<Ressort>();

export interface RessortState extends EntityState<Ressort> {
}

export const initialState: RessortState = ressortAdapter.getInitialState();

export const getRessortState = createFeatureSelector<RessortState>('ressort');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = ressortAdapter.getSelectors(getRessortState);

export const selectRessort = id => {
  return createSelector(selectAll, x => x.find(ressort => ressort.id === id));
};

export const selectRessortsAlphabetically = () => {
  return createSelector(selectAll, x => x.sort(sortAlphabetically()));
};

function sortAlphabetically() {
  return (a, b) => a.title.localeCompare(b.title);
}
