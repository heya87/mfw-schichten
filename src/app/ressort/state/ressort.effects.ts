import {Injectable} from '@angular/core';
import * as RessortActions from './ressort.action';
import {RessortService} from '../service/ressort.service'
import {Actions, Effect, ofType} from '@ngrx/effects';
import {flatMap} from 'rxjs/operators';


@Injectable()
export class RessortEffects {

  constructor(private actions$: Actions, private ressortService: RessortService) {
  }

  @Effect({dispatch: false})
  add$ = this.actions$.pipe(
    ofType(RessortActions.AddRessort),
    flatMap(data => {
      return this.ressortService.addRessort(data.title, data.description, data.email, data.visibility, data.additionalFields);
    })
  );

  @Effect({dispatch: false})
  update$ = this.actions$.pipe(
    ofType(RessortActions.UpdateRessort),
    flatMap(data => {
      return this.ressortService.updateRessort(data.ressort);
    })
  );

  @Effect({dispatch: false})
  remove$ = this.actions$.pipe(
    ofType(RessortActions.RemoveRessort),
    flatMap(data => {
      return this.ressortService.removeRessort(data.id);
    }),
  );
}
