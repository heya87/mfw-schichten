import {createAction, props} from "@ngrx/store";
import {Ressort} from '../../models/ressorts';
import {AdditionalFields} from '../../models/additionalFields';


const GET = '[Ressort] Get'
const RESSORTS_CHANGED = '[Ressort] Changed, reading.'
const BEGIN_ADD = '[Ressort] Begin Add'
const UPDATE = '[Ressort] Update'
const REMOVE = '[Ressort] Remove'
const ERROR = '[Ressort] Error'

export const RessortsChanged = createAction(RESSORTS_CHANGED, props<{ ressorts: Ressort[] }>());

export const GetRessort = createAction(GET, props<{ ressort: Ressort }>());

export const AddRessort = createAction(BEGIN_ADD, props<{title: string, description: string, email: string, visibility: boolean, additionalFields: AdditionalFields[]}>());

export const UpdateRessort = createAction(UPDATE, props<{ ressort: Ressort }>());

export const RemoveRessort = createAction(REMOVE, props<{ id: string }>());

export const ErrorRessort = createAction(ERROR, props<Error>());
