export const ressortRoutesNames = {
  BASE: '',
  CREATE: 'ressort/create',
  DETAIL: 'ressort/details/:id',
  EDIT: 'ressort/edit/:id'
};
