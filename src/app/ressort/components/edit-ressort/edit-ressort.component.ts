import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as RessortActions from '../../state/ressort.action';
import * as fromRessort from '../../state/ressort.state';
import {Ressort} from '../../../models/ressorts';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ressortRoutesNames} from '../../ressorts.route.names';
import {additionalFieldInitData} from '../../../shared/additionalFields';
import {AdditionalFields} from '../../../models/additionalFields';

@Component({
  selector: 'app-edit-ressort',
  templateUrl: './edit-ressort.component.html'
})
export class EditRessortComponent implements OnInit {

  ressortForm: FormGroup;

  public additionalFieldInitData = additionalFieldInitData;

  @Input('ressort')
  public ressort: Ressort;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromRessort.RessortState>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {

    const ressortFormGroup = {
      title: new FormControl(this.ressort.title, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]),
      description: new FormControl( this.ressort.description, [Validators.required, Validators.minLength(3), Validators.maxLength(3000)]),
      visibility: new FormControl(this.ressort.visibility, [Validators.required]),
      email: new FormControl(this.ressort.email, [Validators.minLength(3), Validators.maxLength(30)])
    };

    this.additionalFieldInitData.forEach( field => {
      ressortFormGroup[field.id] = new FormControl(
        this.ressort.additionalFields ?
          this.ressort.additionalFields.find(findField => findField.id === field.id) ?
            this.ressort.additionalFields.find(findField => findField.id === field.id).selected
          : false
        : false
      );
    });

    this.ressortForm = new FormGroup(ressortFormGroup);

  }

  updateRessort(id: string, title: string, description: string, email: string, visibility: boolean, additionalFields: AdditionalFields[]) {
    let ressort: Ressort;
    ressort = {
      ...this.ressort,
      title,
      description,
      email,
      visibility,
      additionalFields
    };
    this.store.dispatch(RessortActions.UpdateRessort({ressort}));
  }

  public get titleControl(): AbstractControl {
    return this.ressortForm.get('title');
  }

  public get descriptionControl(): AbstractControl {
    return this.ressortForm.get('description');
  }

  public get additionalFieldsControl(): AbstractControl {
    return this.ressortForm.get('additionalFields');
  }

  public get emailControl(): AbstractControl {
    return this.ressortForm.get('email');
  }

  public get errorMessageTitle() {
    let errorMessage: string;

    if (this.titleControl.hasError('required')) {
      errorMessage = 'Bitte einen Titel angeben';
    } else if (this.titleControl.hasError('maxlength')) {
      errorMessage = 'Der Titel ist zu lang. (Maximal 50 Zeichen)';
    } else if (this.titleControl.hasError('minlength')) {
      errorMessage = 'Der Titel ist zu kurz. (Minimal 3 Zeichen)';
    }
    return errorMessage;
  }

  public get errorMessageDescription() {
    let errorMessage: string;

    if (this.descriptionControl.hasError('required')) {
      errorMessage = 'Bitte eine Beschreibung für das Ressort angeben';
    } else if (this.descriptionControl.hasError('maxlength')) {
      errorMessage = 'Die Beschreibung ist zu lang. (Maximal 300 Zeichen)';
    } else if (this.descriptionControl.hasError('minlength')) {
      errorMessage = 'Die Beschreibung ist zu kurz. (Minimal 3 Zeichen)';
    }
    return errorMessage;
  }


  public get errorMessageEmail() {
    let errorMessage: string;

    if (this.titleControl.hasError('maxlength')) {
      errorMessage = 'Der Titel ist zu lang. (Maximal 50 Zeichen)';
    } else if (this.titleControl.hasError('minlength')) {
      errorMessage = 'Der Titel ist zu kurz. (Minimal 3 Zeichen)';
    }
    return errorMessage;
  }


  async onSubmit(id: string) {
    this.ressortForm.disable();

    let additionalFieldData = this.ressort.additionalFields;

    if (additionalFieldData) {
      additionalFieldData.forEach(additionalField => {
        additionalField.selected = this.ressortForm.value[additionalField.id] ? this.ressortForm.value[additionalField.id] : false;
      });
    } else {
      additionalFieldData = additionalFieldInitData;
    }


    this.updateRessort(
      id,
      this.ressortForm.value.title,
      this.ressortForm.value.description,
      this.ressortForm.value.email,
      this.ressortForm.value.visibility,
      additionalFieldData
    );
    this.ressortForm.enable();
    this.navigateToDetailView(id);
  }

  isInvalid(value) {
    return this.ressortForm.controls[value].invalid
      && (this.ressortForm.controls[value].dirty || this.ressortForm.controls[value].touched);
  }

  private navigateToDetailView(id: string) {
    const path = ressortRoutesNames.DETAIL.replace(':id', id);
    this.router.navigate([path]);
  }
}


@Component({
  selector: 'app-edit-ressort-wrapper',
  template: `
      <app-edit-ressort *ngIf="ressort$ | async" [ressort]="ressort$ | async"></app-edit-ressort>
  `,
})

export class EditRessortWrapperComponent implements OnInit {
  public ressort$: Observable<Ressort>;

  constructor(private store: Store<fromRessort.RessortState>, private route: ActivatedRoute) {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.ressort$ = this.store.select(
        fromRessort.selectRessort(id));
    }
  }

  ngOnInit() {
  }
}
