import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import * as RessortActions from '../../state/ressort.action';
import * as fromRessort from '../../state/ressort.state';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {additionalFieldInitData} from '../../../shared/additionalFields';

@Component({
  selector: 'app-create-ressort',
  templateUrl: './create-ressort.component.html'
})
export class CreateRessortComponent implements OnInit {

  public hide = true;

  public additionalFieldInitData = additionalFieldInitData;

  ressortForm: FormGroup;
  @ViewChild('titleInput', {static: true}) private titleInput: ElementRef;
  @ViewChild('descriptionInput', {static: true}) private descriptionInput: ElementRef;
  @ViewChild('visibilityInput', {static: true}) private visibilityInput: ElementRef;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromRessort.RessortState>
  ) {
  }

  ngOnInit() {
    const ressortFormGroup = {
      title: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]),
      description: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(3000)]),
      visibility: new FormControl(false, [Validators.required]),
      email: new FormControl('', [Validators.minLength(3), Validators.maxLength(30)]),
    };

    this.additionalFieldInitData.forEach( field => {
      ressortFormGroup[field.id] = new FormControl('');
    });

    this.ressortForm = new FormGroup(ressortFormGroup, {updateOn: 'change'});

  }


  createRessort(title, description, email, visibility, additionalFields) {
    this.store.dispatch(RessortActions.AddRessort({title, description, email, visibility, additionalFields}));
  }

  public get titleControl(): AbstractControl {
    return this.ressortForm.get('title');
  }

  public get emailControl(): AbstractControl {
    return this.ressortForm.get('email');
  }

  public get descriptionControl(): AbstractControl {
    return this.ressortForm.get('description');
  }

  public get errorMessageEmail() {
    let errorMessage: string;

    if (this.titleControl.hasError('maxlength')) {
      errorMessage = 'Der Titel ist zu lang. (Maximal 50 Zeichen)';
    } else if (this.titleControl.hasError('minlength')) {
      errorMessage = 'Der Titel ist zu kurz. (Minimal 3 Zeichen)';
    }
    return errorMessage;
  }

  public get errorMessageTitle() {
    let errorMessage: string;

    if (this.titleControl.hasError('required')) {
      errorMessage = 'Bitte einen Titel angeben';
    } else if (this.titleControl.hasError('maxlength')) {
      errorMessage = 'Der Titel ist zu lang. (Maximal 50 Zeichen)';
    } else if (this.titleControl.hasError('minlength')) {
      errorMessage = 'Der Titel ist zu kurz. (Minimal 3 Zeichen)';
    }
    return errorMessage;
  }

  public get errorMessageDescription() {
    let errorMessage: string;

    if (this.descriptionControl.hasError('required')) {
      errorMessage = 'Bitte eine Beschreibung für das Ressort angeben';
    } else if (this.descriptionControl.hasError('maxlength')) {
      errorMessage = 'Die Beschreibung ist zu lang. (Maximal 300 Zeichen)';
    } else if (this.descriptionControl.hasError('minlength')) {
      errorMessage = 'Die Beschreibung ist zu kurz. (Minimal 3 Zeichen)';
    }
    return errorMessage;
  }


  async onSubmit() {

    this.ressortForm.disable();

    const additionalFieldData = additionalFieldInitData;

    additionalFieldData.forEach(additionalField => {
      additionalField.selected = this.ressortForm.value[additionalField.id] ? this.ressortForm.value[additionalField.id] : false;
    });

    this.createRessort(
        this.ressortForm.value.title,
        this.ressortForm.value.description,
        this.ressortForm.value.email,
        this.ressortForm.value.visibility,
        additionalFieldData
    );
    this.titleInput.nativeElement.value = '';
    this.descriptionInput.nativeElement.value = '';
    this.ressortForm.enable();
  }

}
