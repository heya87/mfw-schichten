import {Component, Input, OnInit} from '@angular/core';
import * as fromRessort from '../../state/ressort.state';
import * as fromCore from '../../../core/state/core.state';
import * as fromShift from '../../../shift/state/shift.state';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {ressortRoutesNames} from '../../ressorts.route.names';
import {Ressort} from '../../../models/ressorts';
import {User} from '../../../models/user';
import * as CoreActions from '../../../core/state/core.action';
import {userProfileRoutesNames} from '../../../user-profile/user-profile.route.names';

@Component({
  selector: 'app-ressorts-overview',
  templateUrl: './ressorts-overview.component.html',
  styleUrls: ['./ressorts-overview.component.scss']

})
export class RessortsOverviewComponent implements OnInit {

  @Input('user')
  public user: User;

  @Input('ressorts')
  public ressorts: Ressort[];

  @Input('isAdmin')
  public isAdmin: boolean;

  @Input('isLoggedin')
  public isLoggedin: boolean;

  columns: number;

  constructor(
    public router: Router,
    private ressortStore: Store<fromRessort.RessortState>,
    private coreStore: Store<fromCore.State>,
    private shiftStore: Store<fromShift.ShiftState>,
  ) {

  }

  ngOnInit() {
    this.setBreakpoint(window.innerWidth);
  }

  async updateUserLastRessort(ressort) {

    const user: User = {
      ...this.user,
      lastRessort: ressort.id
    };
    return this.coreStore.dispatch(CoreActions.UpdateMetadataForUser({user}));
  }

  onResize(event) {
    this.setBreakpoint(event.target.innerWidth);
  }

  private setBreakpoint(windowWith: number) {
    if (windowWith > 1000) {
      this.columns = 3;
    } else if (windowWith > 800 && windowWith <= 1000) {
      this.columns = 2;
    } else {
      this.columns = 1;
    }
  }

  navigateToRessort(id: string) {
    const path = ressortRoutesNames.DETAIL.replace(':id', id);
    this.router.navigate([path]);
  }

  navigateToEditAdditionalFields(id: string) {
    const path = userProfileRoutesNames.EDIT_ADDITIONAL_FULL.replace(':uid', id);
    this.router.navigate([path]);
  }

  isUserInfoSufficient(ressort: Ressort) {

    const isRequiredFieldsProvided = ressort.additionalFields.find(additionalField => additionalField.selected && !this.user[additionalField.id]);

    if (isRequiredFieldsProvided) {
      this.updateUserLastRessort(ressort);
      this.navigateToEditAdditionalFields(this.user.uid);
    } else {
      this.navigateToRessort(ressort.id);
    }
  }

  getCountNotConfirmedShifts(ressortId: any): Observable<number> {
    return this.shiftStore.select((fromShift.selectCountNotConfirmedShifts(ressortId)));
  }
}


@Component({
  selector: 'app-ressorts-overview-wrapper',
  template: `
    <app-ressorts-overview [user]="user$ | async" [ressorts]="ressorts$ | async" [isAdmin]="isAdmin$ | async" [isLoggedin]="isLoggedIn$ | async"></app-ressorts-overview>
  `

})
export class RessortsOverviewWrapperComponent implements OnInit {

  ressorts$: Observable<Ressort[]>;
  isAdmin$: Observable<boolean>;
  user$: Observable<User>;
  isLoggedIn$: Observable<boolean>;

  constructor(
    private ressortStore: Store<fromRessort.RessortState>,
    private coreStore: Store<fromCore.State>,
  ) {

  }


  ngOnInit() {
    this.ressorts$ = this.ressortStore.select(fromRessort.selectRessortsAlphabetically());
    this.isAdmin$ = this.coreStore.select(fromCore.isAdmin);
    this.isLoggedIn$ = this.coreStore.select(fromCore.isLoggedIn);
    this.user$ = this.coreStore.select(fromCore.selectMetadata);
  }

}

