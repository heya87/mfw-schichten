import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CalendarComponent} from './components/calendar/calendar.component';
import {ShiftModule} from '../shift/shift.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ShiftsPerDayComponent} from './components/shifts-per-day-list/shifts-per-day.component';
import {MatListModule} from '@angular/material/list';
import {ShiftsPerMonthComponent} from './components/shifts-per-month-list/shifts-per-month.component';
import {MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  declarations: [CalendarComponent, ShiftsPerDayComponent, ShiftsPerMonthComponent],
  exports: [
    CalendarComponent,
    ShiftsPerDayComponent
  ],
  imports: [
    CommonModule,
    ShiftModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatListModule,
    MatTooltipModule
  ]
})
export class CalendarModule {
}
