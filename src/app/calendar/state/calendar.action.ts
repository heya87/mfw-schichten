import {createAction, props} from "@ngrx/store";


const GET = '[Calendar] Get';
const UPDATE_DAYS = '[Calendar] Update days';
const ERROR = '[Calendar] Error';
const NEXT_DATE = '[Calendar] Next date';
const PREV_DATE = '[Calendar] Prev date';
const NEXT_MONTH = '[Calendar] Next month';
const PREV_MONTH = '[Calendar] Prev month';
const NO_OF_DAYS = '[Calendar] Set number of days';


export const GetDays = createAction(GET);

export const UpdateDays = createAction(UPDATE_DAYS, props<{dates: Date[] }>());
export const NextDate = createAction(NEXT_DATE);
export const PrevDate = createAction(PREV_DATE);
export const NextMonth = createAction(NEXT_MONTH);
export const PrevMonth = createAction(PREV_MONTH);
export const NoOfDays = createAction(NO_OF_DAYS, props<{ noOfDays: number }>());

export const ErrorCalendar = createAction(ERROR, props<Error>());
