import {Action, createReducer, on} from '@ngrx/store';
import * as calendarActions from './calendar.action';
import {calendarAdapter, CalendarState, initialState} from './calendar.state';
import {Day} from '../../models/day';

export function calendarReducer(state: CalendarState | undefined, action: Action) {
  return reducer(state, action);
}


const reducer = createReducer(
  initialState,
  on(calendarActions.GetDays, state => state),
  on(calendarActions.UpdateDays, (state, {dates}) => {
    const days: Day[] = [];
    dates.forEach(date => {
      const day: Day = {id: date.getTime().toString(), date};
      days.push(day);
    });
    const emptyState = calendarAdapter.removeAll(state);
    return calendarAdapter.addMany(days, emptyState);
  }),
  on(calendarActions.NextDate, (state) => {
    return appendDay(state, true);
  }),
  on(calendarActions.PrevDate, (state) => {
    return prependDay(state, true);
  }),
  on(calendarActions.NextMonth, (state) => {
    return changeMonth(state, true);
  }),
  on(calendarActions.PrevMonth, (state) => {
    return changeMonth(state, false);
  }),
  on(calendarActions.NoOfDays, (state, {noOfDays}) => {
    if (state.ids.length < noOfDays) {
      return addDays(state, (noOfDays - state.ids.length));
    } else if (state.ids.length > noOfDays) {
      return removeDays(state, (state.ids.length - noOfDays));
    } else {
      return state;
    }
  }),
);

function changeMonth(state, increment: boolean) {

  const lastIndex = state.ids.length - 1;
  const idLastDay = state.ids[lastIndex];
  const lastDay = state.entities[idLastDay];
  let monthToAdd;

  const emptyState = calendarAdapter.removeAll(state);
  if (increment) {
    monthToAdd = incrementMonth(lastDay.date, 1);
  } else {
    monthToAdd = decrementMonth(lastDay.date, 1);
  }

  const dayToAdd: Day = {id: monthToAdd.getTime().toString(), date: monthToAdd};

  const dates = [];
  dates.push(dayToAdd);
  const updated = calendarAdapter.addMany(dates, emptyState)

  updated.ids.sort(sortIds);
  return updated;
}

function addDays(state, daysToAdd: number) {
  for (let i = 0; i < daysToAdd; i++) {
    state = appendDay(state, false);
  }
  return state;
}

function removeDays(state, daysToAdd: number) {
  for (let i = 0; i < daysToAdd; i++) {
    state = removeDay(state);
  }
  return state;
}

function removeDay(state) {
  state.ids.sort(sortIds);
  const lastIndex = state.ids.length - 1;
  const updated = calendarAdapter.removeOne(state.ids[lastIndex].toString(), state);
  updated.ids.sort(sortIds);
  return updated;
}

function appendDay(state, removeLast: boolean) {
  state.ids.sort(sortIds);
  const lastIndex = state.ids.length - 1;
  const idLastDay = state.ids[lastIndex];
  const lastDay = state.entities[idLastDay];
  const dateToAdd = incrementDate(lastDay.date, 1);
  const dayToAdd: Day = {id: dateToAdd.getTime().toString(), date: dateToAdd};
  if (removeLast) {
    let updated = calendarAdapter.removeOne(state.ids[0].toString(), state);
    updated = calendarAdapter.addOne(dayToAdd, updated);
    updated.ids.sort(sortIds);
    return updated;
  } else {
    const updated = calendarAdapter.addOne(dayToAdd, state);
    updated.ids.sort(sortIds);
    return updated;
  }
}
function prependDay(state, removeLast: boolean) {
  state.ids.sort(sortIds);
  const lastIndex = state.ids.length - 1;
  const idLastDay = state.ids[lastIndex];
  const idFirstDay = state.ids[0];
  const firstDay = state.entities[idFirstDay];
  const dateToAdd = decrementDate(firstDay.date, 1);
  const dayToAdd: Day = {id: dateToAdd.getTime().toString(), date: dateToAdd};
  if (removeLast) {
    let updated = calendarAdapter.removeOne(idLastDay.toString(), state);
    updated = calendarAdapter.addOne(dayToAdd, updated);
    updated.ids.sort(sortIds);
    return updated;
  } else {
    const updated = calendarAdapter.addOne(dayToAdd, state);
    updated.ids.sort(sortIds);
    return updated;
  }
}

function sortIds(idOne, idTwo) {
  return idOne - idTwo;
}

function decrementMonth(toIncrement: Date, months: number) {
  const result = new Date();
  result.setFullYear(toIncrement.getFullYear())
  result.setMonth(toIncrement.getMonth() - months)
  result.setDate(toIncrement.getDate());
  return result;
}

function incrementMonth(toIncrement: Date, months: number) {
  const result = new Date();
  result.setFullYear(toIncrement.getFullYear())
  result.setMonth(toIncrement.getMonth() + months)
  result.setDate(toIncrement.getDate());
  return result;
}

function incrementDate(toIncrement: Date, days: number) {
  const result = new Date();
  result.setFullYear(toIncrement.getFullYear())
  result.setMonth(toIncrement.getMonth())
  result.setDate(toIncrement.getDate() + days);
  return result;
}

function decrementDate(toDecrement: Date, days: number) {
  const result = new Date();
  result.setFullYear(toDecrement.getFullYear())
  result.setMonth(toDecrement.getMonth())
  result.setDate(toDecrement.getDate() - days);
  return result;
}
