import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromCalendar from '../../state/calendar.state';
import * as CalendarActions from '../../state/calendar.action';
import {Observable} from 'rxjs';
import {Ressort} from '../../../models/ressorts';
import {Day} from '../../../models/day';
import {Shift} from '../../../models/shift';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})

export class CalendarComponent implements OnInit {

  @Input('ressort')
  public ressort: Ressort;

  @Input('shifts')
  public shifts: Shift[];

  calendar$: Observable<Day[]>;

  public firstShiftDate: Date;

  constructor(
    private calendarStore: Store<fromCalendar.CalendarState>,
  ) {}

  ngOnInit() {

    const dates: Date[] = [];
    if (this.shifts.length > 0) {
      this.firstShiftDate = new Date(this.shifts[0].start.seconds * 1000);
      dates.push(this.firstShiftDate);
    } else {
      dates.push(new Date());
    }

    this.calendarStore.dispatch(CalendarActions.UpdateDays({dates}));
    this.calendar$ = this.calendarStore.select(fromCalendar.selectAll);
  }
}

