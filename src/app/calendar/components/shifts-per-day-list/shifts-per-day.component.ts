import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromShift from '../../../shift/state/shift.state';
import {Observable} from 'rxjs';
import {Shift} from '../../../models/shift';
import {Day} from '../../../models/day';
import * as CalendarActions from '../../state/calendar.action';
import * as fromCore from '../../../core/state/core.state';

@Component({
  selector: 'app-shifts-per-day',
  templateUrl: './shifts-per-day.component.html',
})
export class ShiftsPerDayComponent implements OnInit {

  @Input('ressortId')
  public ressortId: string;

  @Input('ressortTitle')
  public ressortTitle: string;

  @Input('day')
  public day: Day;

  public isAdmin$: Observable<boolean>;

  shifts$: Observable<Shift[]>;

  constructor(
    private shiftStore: Store<fromShift.ShiftState>,
    private coreStore: Store<fromCore.State>,
  ) {
  }

  ngOnInit() {
    this.shifts$ = this.shiftStore.select(fromShift.selectShiftsForRessortAndDate(this.ressortId, this.day.date));
    this.isAdmin$ = this.coreStore.select(fromCore.isAdmin);
  }


  nextDate() {
    this.shiftStore.dispatch(CalendarActions.NextDate());
  }

  prevDate() {
    this.shiftStore.dispatch(CalendarActions.PrevDate());
  }
}
