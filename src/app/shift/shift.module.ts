import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShiftsRoutingModule} from './shifts-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {CreateShiftComponent} from './components/create-shift/create-shift.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {shiftReducer} from './state/shift.reducer';
import {ShiftEffects} from './state/shift.effects';
import {MatListModule} from '@angular/material/list';
import {ShiftListComponent} from './components/shift-list/shift-list.component';
import {ShiftDetailComponent} from './components/shift-detail/shift-detail.component';
import {UsersWorkerListComponent} from '../users/components/users-worker-list/users-worker-list.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {UsersExportComponent, UsersExportWrapperComponent} from '../shared/users-export/users-export.component';
import {BackButtonModule} from './../shared/back-button/back-button.module';
import {MatButtonModule, MatIconModule} from '@angular/material';
import {ExportShiftPerDayComponent} from "./components/export-shifts-per-day/export-shift-per-day.component";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {EditShiftComponent} from './components/edit-shift/edit-shift.component';

@NgModule({
  declarations: [
    CreateShiftComponent,
    ShiftListComponent, ShiftListComponent,
    ShiftDetailComponent,
    UsersWorkerListComponent,
    UsersExportComponent,
    UsersExportWrapperComponent,
    ExportShiftPerDayComponent,
    EditShiftComponent
  ],
  exports: [
    CreateShiftComponent,
    ShiftListComponent,
    ShiftDetailComponent,
    UsersWorkerListComponent,
    UsersExportComponent,
    UsersExportWrapperComponent,
    ExportShiftPerDayComponent,
    EditShiftComponent
  ],
  imports: [
    StoreModule.forFeature('shift', shiftReducer),
    EffectsModule.forFeature([ShiftEffects]),
    FlexLayoutModule,
    CommonModule,
    ShiftsRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FlexModule,
    MatListModule,
    MatTooltipModule,
    BackButtonModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  providers: [
  ],
})
export class ShiftModule {
}
