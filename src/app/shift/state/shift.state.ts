import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Shift} from '../../models/shift';
import {State} from '../../core/state/core.state';

export const shiftAdapter = createEntityAdapter<Shift>();

export interface ShiftState extends EntityState<Shift> {
}

export const initialState: ShiftState = shiftAdapter.getInitialState();

export const getShiftState = createFeatureSelector<ShiftState>('shift');
export const getCoreState = createFeatureSelector<State>('core');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = shiftAdapter.getSelectors(getShiftState);

export const selectShiftsById = shiftId => {
  return createSelector(selectAll, x => x.find(shift => shift.id === shiftId));
};

export const selectIsAdmin = createSelector(
  getCoreState,
  state => state.metadata && state.metadata.isAdmin
);

export const selectShiftsForRessortAndDate = (ressortId, date) => {
  return createSelector(selectAll, selectIsAdmin, (shifts, isAdmin) => {
    return selectShiftsForUserRole(isAdmin, shifts)
      .filter(shift => isRessortAndDateMatch(ressortId, date, shift))
      .sort(sortShiftByStartDate());
  });
};

export const selectShiftsForRessort = (ressortId) => {
  return createSelector(selectAll, selectIsAdmin, (shifts, isAdmin) => {
    return selectShiftsForUserRole(isAdmin, shifts)
      .filter(shift => shift.ressortId === ressortId)
      .sort(sortShiftByStartDate());
  });
};


export const selectShiftsForUser = (userId, confirmed) => {
  return createSelector(selectAll, x => {
    return x.filter(shift => isUserMatch(userId, confirmed, shift)).sort(sortShiftByStartDate());
  });
};

export function selectCountNotConfirmedShifts(ressortId: any) {
  return createSelector(selectAll, x => {
    const shifts = x.filter(shift => shift.ressortId === ressortId && !shift.confirmed);
    return sumUpOpenShifts(shifts);
  });
}

const selectShiftsForUserRole = (isAdmin, shifts) => {
  if (!isAdmin) {
    return shifts.filter(shift => {
      return (
        !shift.confirmed &&
        shift.noOfWorker > shift.workers.length
      );
    });
  }
  return shifts;
};

function isRessortAndDateMatch(ressortId: string, date: Date, shift: Shift): boolean {
  const shiftDate = shift.start.toDate();

  return (
    ressortId === shift.ressortId &&
    shiftDate.getDate() === date.getDate() &&
    shiftDate.getMonth() === date.getMonth() &&
    shiftDate.getFullYear() === date.getFullYear()
  );
}

function isUserMatch(userId: string, confirmed: boolean, shift: Shift): boolean {
  return (
    shift.workers &&
    shift.workers.some(worker => {
      return (
        worker.id === userId &&
        worker.confirmed === confirmed
      );
    })
  );
}

function sortShiftByStartDate() {
  return (a, b) => {
    return a.start.seconds - b.start.seconds;
  };
}

function sumUpOpenShifts(shifts: Shift[]) {
  let sum = 0;
  shifts.forEach(shift => {
    sum = sum + (shift.noOfWorker - shift.workers.length);
  });
  return sum;
}

