import {createAction, props} from "@ngrx/store";
import {Shift} from '../../models/shift';

const GET = '[Shift] Get'
const SHIFTS_CHANGED = '[Shift] Changed, reading.'
const ADD = '[Shift] Add'
const ADD_SHIFT_WORKER = '[Shift] Add'
const UPDATE = '[Shift] Update'
const UPDATE_WORKERS_WITH_LIMIT_CHECK = '[Shift] Update workers with limit check'
const REMOVE = '[Shift] Remove'
const REMOVE_SHIFTS_FOR_RESSORT = '[Shift] Remove shifts for ressort'
const ERROR = '[Shift] Error'

export const ShiftsChanged = createAction(SHIFTS_CHANGED, props<{ shifts: Shift[] }>());

export const GetShift = createAction(GET, props<{ shift: Shift }>());

export const AddShift = createAction(ADD, props<{ shift: Shift }>());

export const UpdateShift = createAction(UPDATE, props<{ shift: Shift }>());

export const UpdateWorkersWithLimitCheck = createAction(UPDATE_WORKERS_WITH_LIMIT_CHECK, props<{ shift: Shift }>());

export const AddShiftWorker = createAction(ADD_SHIFT_WORKER, props<{ shift: Shift, workerId: string }>());

export const RemoveShift = createAction(REMOVE, props<{ id: string }>());

export const  RemoveShiftsForRessort = createAction(REMOVE_SHIFTS_FOR_RESSORT, props<{ ressortId: string }>());

export const ErrorShift = createAction(ERROR, props<Error>());
