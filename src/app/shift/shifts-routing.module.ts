import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShiftDetailComponent} from './components/shift-detail/shift-detail.component';

const routes: Routes = [
  {
    path: '', component: ShiftDetailComponent,
  },
  {
    path: '', component: ShiftDetailComponent, pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiftsRoutingModule {
}
