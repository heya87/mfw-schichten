import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import * as ShiftActions from '../../state/shift.action';
import {Store} from '@ngrx/store';
import * as fromShift from '../../state/shift.state';
import {Shift} from '../../../models/shift';
import {min} from 'rxjs/operators';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase';



@Component({
  selector: 'app-edit-shift',
  templateUrl: './edit-shift.component.html'
})
export class EditShiftComponent implements OnInit {

  @Input()
  public shift: Shift;
  shiftForm: FormGroup;
  @ViewChild('noOfWorkerInput', {static: true}) public noOfWorkerInput: ElementRef;
  @ViewChild('shiftEndInput', {static: true}) public shiftEndInput: ElementRef;
  private minNoOfWorkers = 1;


  constructor(private fb: FormBuilder, private store: Store<fromShift.ShiftState>) {
  }

  ngOnInit() {
    this.shiftForm = this.fb.group({
        noOfWorker: new FormControl(this.shift.noOfWorker, [Validators.required, Validators.min(this.minNoOfWorkers)]),
        title: new FormControl(this.shift.title),
        shiftStart: new FormControl(new Date(this.shift.start.seconds * 1000), [Validators.required]),
        shiftEnd: new FormControl(new Date(this.shift.end.seconds * 1000), [Validators.required]),
      }, {
        validators: [this.isEndShiftAfterStartShift, this.hasEnoughSlots],
        updateOn: 'change'
      },
    );
  }

  private isEndShiftAfterStartShift: ValidatorFn = (control: FormGroup): ValidationErrors | null => {

    const shiftStart = control.get('shiftStart');
    const shiftEnd = control.get('shiftEnd');

    return shiftEnd && shiftStart && shiftEnd.value && shiftEnd.value < shiftStart.value ? { backToTheFuture: true } : null;
  }

  private hasEnoughSlots: ValidatorFn = (control: FormGroup): ValidationErrors | null => {

    const noOfWorker = control.get('noOfWorker');

    return noOfWorker && this.shift.workers.length > noOfWorker.value ? { notEnoughSlots: true } : null;
  }


  public get noOfWorkerControl(): AbstractControl {
    return this.shiftForm.get('noOfWorker');
  }

  public get shiftStartControl(): AbstractControl {
    return this.shiftForm.get('shiftStart');
  }

  public get shiftEndControl(): AbstractControl {
    return this.shiftForm.get('shiftEnd');
  }

  public get errorMessageNoOfWorker() {
    let errorMessage = 'Ungültiger Wert';

    if (this.noOfWorkerControl.hasError('required')) {
      errorMessage = 'Bitte die Anzahl Arbeiter angeben.';
    } else if (this.noOfWorkerControl.hasError('min')) {
      errorMessage = 'Minimale Anzahl Arbeiter: ' + this.minNoOfWorkers;
    }
    return errorMessage;
  }

  public get errorMessageStart() {
    let errorMessage = 'Ungültiger Wert';

    if (this.noOfWorkerControl.hasError('required')) {
      errorMessage = 'Bitte Schichtstart auswählen.';
    }
    return errorMessage;
  }

  public get errorMessageEnd() {
    let errorMessage = 'Ungültiger Wert';

    if (this.noOfWorkerControl.hasError('required')) {
      errorMessage = 'Bitte Schicht-Ende auswählen.';
    }
    return errorMessage;
  }

  async submit() {
    this.shiftForm.disable();
    await this.createShift(
      this.shiftForm.value.noOfWorker,
      Timestamp.fromDate(this.shiftForm.value.shiftStart),
      Timestamp.fromDate(this.shiftForm.value.shiftEnd),
      this.shiftForm.value.title
    );
    this.shiftForm.enable();
  }


  createShift(noOfWorker: number, start: Timestamp, end: Timestamp, title: string = '') {
    let shift: Shift;
    shift = {
      ...this.shift,
      start,
      end,
      noOfWorker,
      confirmed: false,
      title
    };
    this.store.dispatch(ShiftActions.UpdateShift({shift}));
  }
}
