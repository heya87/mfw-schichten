import {Component, Input, OnInit} from '@angular/core';
import {Shift} from '../../../models/shift';
import {Store} from '@ngrx/store';
import * as fromShift from '../../state/shift.state';
import * as fromRessort from '../../../ressort/state/ressort.state';
import * as ShiftActions from '../../state/shift.action';
import {ConfirmDialogInformation} from '../../../models/confirmDialogInformation';
import {ConfirmationDialogComponent} from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import * as fromCore from '../../../core/state/core.state';
import {shiftRoutesNames} from '../../shift.route.names';
import {Router} from '@angular/router';
import {User} from '../../../models/user';
import {Observable} from 'rxjs';
import {authRoutesNames} from '../../../authentication/auth.route.names';

@Component({
  selector: 'app-shift-list',
  templateUrl: './shift-list.component.html',
  styleUrls: ['./shift-list.component.scss']
})
export class ShiftListComponent implements OnInit {

  @Input('shifts')
  shifts: Shift[];

  @Input('ressortTitle')
  public ressortTitle: string;

  @Input('showDate')
  public showDate: boolean;

  @Input('showRessort')
  public showRessort: boolean;

  public user$: Observable<User>;
  public isLoggedIn$: Observable<boolean>;
  public isAdmin$: Observable<boolean>;
  public worker: {id: string, confirmed: boolean};

  constructor(
    private shiftStore: Store<fromShift.ShiftState>,
    private coreStore: Store<fromCore.State>,
    private ressortStore: Store<fromRessort.RessortState>,
    public dialog: MatDialog,
    public router: Router,
  ) {
  }

  ngOnInit() {
    this.user$ = this.coreStore.select(fromCore.selectMetadata);
    this.isLoggedIn$ = this.coreStore.select(fromCore.isLoggedIn);
    this.isAdmin$ = this.coreStore.select(fromCore.isAdmin);
  }

  navigateToShiftDetail(id) {
    const path = shiftRoutesNames.DETAIL.replace(':id', id);
    this.router.navigate([path]);
  }

  navigateToLogin() {
    const path = authRoutesNames.SIGN_IN;
    this.router.navigate([path]);
  }

  removeShift(shift: Shift) {
    this.shiftStore.dispatch(ShiftActions.RemoveShift({id: shift.id}));
  }

  public isWorkerRegisteredForShift(shift, userId) {

    return shift.workers.some(worker => worker.id === userId);

  }

  public isWorkerConfirmedForShift(shift, userId) {

    return shift.workers.some(worker => worker.id === userId && worker.confirmed === true);

  }


  private registerForShift(shift, userId) {

    const newWorker = {id: userId, confirmed: false};
    const workers = shift.workers;
    workers.push(newWorker);
    this.updateShiftWorkers(shift, workers);

  }

  private unRegisterFromShift(shift, userId) {
    const workers = shift.workers.filter(worker => worker.id !== userId);
    this.updateShiftWorkers(shift, workers);
  }

  private updateShiftWorkers(shift, workers) {
    let updatedShift: Shift;
    updatedShift = {
      ...shift,
      workers
    };
    this.shiftStore.dispatch(ShiftActions.UpdateWorkersWithLimitCheck({shift: updatedShift}));
  }


  openDialog(ressort: string, shift: Shift, userId: string, register: boolean): void {
    const startDate = new Date(shift.start.toDate());
    const startDateString = startDate.toLocaleDateString(navigator.language);
    const startTimeString = startDate.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
    const endDate = new Date(shift.end.toDate());
    const endTimeString = endDate.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
    const popupTitle = `Ressort ${ressort}`;
    const popupMessageAction = register ? 'anmelden' : 'abmelden';
    const popupMessage = `Willst du dich für diese Schicht am ${startDateString} von ${startTimeString} bis ${endTimeString} ${popupMessageAction}?`;
    const dialogInformation: ConfirmDialogInformation = {
      title: popupTitle,
      message: popupMessage
    };
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: dialogInformation,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (register) {
          this.registerForShift(shift, userId);
        } else {
          this.unRegisterFromShift(shift, userId);
        }
      }
    });
  }

  getRessort(ressortId: string) {
    return this.ressortStore.select(fromRessort.selectRessort(ressortId));
  }
}
