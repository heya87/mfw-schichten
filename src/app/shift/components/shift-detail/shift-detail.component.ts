import {Component, OnInit} from '@angular/core';
import * as ShiftActions from '../../state/shift.action';
import {Store} from '@ngrx/store';
import * as fromShift from '../../state/shift.state';
import {Shift} from '../../../models/shift';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {ConfirmDialogInformation} from '../../../models/confirmDialogInformation';
import {ConfirmationDialogComponent} from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material';
import {UsersService} from '../../../users/service/users.service';


@Component({
  selector: 'app-shift-detail',
  templateUrl: './shift-detail.component.html'
})
export class ShiftDetailComponent implements OnInit {

  public shift$: Observable<Shift>;

  constructor(
    private store: Store<fromShift.ShiftState>,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private usersService: UsersService

  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.shift$ = this.store.select(fromShift.selectShiftsById(id));
  }

  public toggleConfirmShift(shift) {
    let updatedShift: Shift;
    const toggleConfirmed = !shift.confirmed;
    updatedShift = {
      ...shift,
      confirmed: toggleConfirmed
    };
    this.store.dispatch(ShiftActions.UpdateShift({shift: updatedShift}));
  }

  public openDialog(shift: Shift): void {
    const popupTitle = `Schichtstatus ändern`;
    let popupMessage;
    if (shift.confirmed) {
      popupMessage = 'Willst du die Schicht erneut freigeben? Dadurch können sich Freiwillige wieder anmelden.';
    } else {
      popupMessage = 'Willstu du die Schicht bestätigen? Dadurch können sich keine Freiwilligen mehr anmelden';
    }
    const dialogInformation: ConfirmDialogInformation = {
      title: popupTitle,
      message: popupMessage
    };
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: dialogInformation,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.toggleConfirmShift(shift);
      }
    });
  }
}
