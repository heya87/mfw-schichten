import {Component} from '@angular/core';
import {AuthService} from './authentication/service/auth.service';
import {Router} from '@angular/router';

import {MatSnackBar} from '@angular/material/snack-bar';
import {Store} from '@ngrx/store';
import * as CoreActions from './core/state/core.action';
import * as fromCore from './core/state/core.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'projektZwo';
  private openSnackbar = false;

  constructor(
    private store: Store<fromCore.State>,
    public router: Router,
    public authService: AuthService,
    public snackBar: MatSnackBar
  ) {

    store.select(fromCore.selectMessages).subscribe(messages => {
      if (messages.length > 0 && !this.openSnackbar) {
        this.openSnackBar(messages[0], messages[0].message);
      }
    });
  }

  openSnackBar(errorObj: any, message: string, action?: string) {
    this.openSnackbar = true;
    this.snackBar.open(message, action, {
      duration: 5000,
      verticalPosition: 'bottom',
      horizontalPosition: 'right',
    }).afterDismissed().subscribe(() => {
      this.openSnackbar = false;
      this.store.dispatch(CoreActions.RemoveMessage({item: errorObj}));
    });
  }
}
