import {AppPage} from './app.po';
import {browser, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display mithelfen title when going to start page and not be logged in', () => {
    page.navigateTo().then(() => {
      expect(page.getTitleTextRessortOverview()).toEqual('MITHELFEN');
    });
  });

  it('should open Ressort Kasse and navigate back to overview', () => {
    page.navigateTo();
    page.clickRessortKasse().then(() => {
      expect(page.getTitleTextRessortDetail()).toEqual('KASSE');
      page.clickButtonBack().then(() => {
        expect(page.getTitleTextRessortOverview()).toEqual('MITHELFEN');
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
