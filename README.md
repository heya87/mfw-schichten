# Title: mfw-schichten

Second of two exercise projects for the CAS Frontend Engineering course.

## Goal
A lot of events like open airs or or festivals have all kinds of departments like food stand or information.
These must be staffed with workers for the duration of the whole event.
The mfw-schichten app helps to manage the departments, shifts and workers.

## Installation

```
git clone https://gitlab.com/heya87/mfw-schichten.git
cd mfw-schichten
npm install
ng serve
```
Applikation ist unter 
https://localhost:4200/ 
verfügbar.

## Documentation

[Wiki mfw-schichten](https://gitlab.com/heya87/mfw-schichten/wikis/home)